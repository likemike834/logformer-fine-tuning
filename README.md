# Longformer fine-tuning

You can review code in the notebook file:  
[Fine_tuning_LONGFORMER.ipynb](Fine_tuning_LONGFORMER.ipynb)

## Task

Fine-tune NLP model for multi-label multi-class classification.

## Dataset

62000 Youtube videos' subtitles with 60 class labels.   
Data was augmented with nlpaug.

## Results

![AUC-ROC 0.99 reached](img/AUC-ROC.png "AUC-ROC results")

AUC-ROC 0.99 reached vs 0.80 without fine-tuning
